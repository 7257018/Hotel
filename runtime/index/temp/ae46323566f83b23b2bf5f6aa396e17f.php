<?php /*a:3:{s:50:"G:\phpstudy_pro\WWW\tp\view\index\citys\index.html";i:1605605083;s:52:"G:\phpstudy_pro\WWW\tp\view\index\common\static.html";i:1602239170;s:55:"G:\phpstudy_pro\WWW\tp\view\index\common\resources.html";i:1601945357;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>
</head>
<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">演示</a>
                <a>
                    <cite>导航元素</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-header">
                            <div class="layui-input-inline layui-show-xs-block">
                                <input type="text" id="city" placeholder="请输入城市" autocomplete="off" class="layui-input">
                            </div>
                            <button class="layui-btn" onclick="adds()">
                                <i class="layui-icon"></i>
                                添加
                            </button>
                        </div>
                        <div class="layui-card-body ">
                            <div class="layui-row">
                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <div class="layui-col-md2"
                                         style="text-align: center;background-color: #71d5a1;margin: 20px;cursor:pointer;">

                                        <button type="button" class="layui-btn layui-btn-sm layui-btn-danger" style="float: right"
                                                onclick="city_del(<?php echo htmlentities($vo['id']); ?>)">
                                            移除
                                        </button>
                                        <span style="color: #FFFFFF"><h3><?php echo htmlentities($vo['citys']); ?></h3></span>
                                    </div>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>

<script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
            });

            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
            });
        });


function adds(){
    $.ajax({
        type:"post",
        url: "<?php echo url('index/citys/adds'); ?>",
        data: {
            citys:$('#city').val()
        },
        success: function(data){
            console.log(data);
            toastr.error(data.msg);
            if(data.code == 100){
                setTimeout(function () {
                    layer.closeAll();
                    parent.location.reload();
                },1500);
            }
        }});
}

/*房型-删除*/
function city_del(id) {

    $.ajax({
        type:"post",
        url: "<?php echo url('index/citys/deletes'); ?>",
        data: {
            id:id
        },
        success: function(data){
            console.log(data);
            toastr.error(data.msg);
            if(data.code == 100){
                setTimeout(function () {
                    location.reload();
                },1000);
            }
        }});

}

</script>


</html>