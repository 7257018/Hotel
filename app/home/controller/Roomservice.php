<?php
namespace app\home\controller;


use app\index\controller\Basics;
use think\facade\Db;


/*
 * 客房服务
 *
 * */

class Roomservice extends Basics
{


    /*
     * 客房服务
     *
     * */
    public function index()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(Db::name('home_room_service')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        $list = Db::table('home_room_service')->where('building_id',session('building_id'))->select();
        return view('index',['list'=>$list]);
    }

    /*
     * 人员管理
     * */
    public function personnel(){

        $list = Db::table('home_room_admin')->where('building_id',session('building_id'))->paginate(10);
        return view('personnel',['list'=>$list]);
    }

    /*
     * 添加人员
     * */
    public function adds_personnel(){

        if(request()->isAjax()){
            $data = input('param.');
            $data['building_id'] = session('building_id');
            if(Db::name('home_room_admin')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        return view();
    }

    /*
     * 派发任务
     * */
    public function room_publish(){
        $list = Db::table('home_room_publish')->where('building_id',session('building_id'))->paginate(10);
        $list =  Db::table('home_room_publish')
            ->alias('a')
            ->field('a.*,b.room_num,c.name,d.service_name')
            ->join('room b','a.room = b.id')
            ->join('home_room_admin c','a.admin = c.id')
            ->join('home_room_service d','a.service = d.id')
            ->where('a.building_id',session('building_id'))
            ->paginate(10);
        dump($list);
        return view('room_publish',['list'=>$list]);
    }

    /*
     * 指定任务
     * */
    public function add_publish(){

        if(request()->isAjax()){
            $data = input('param.');
            $data['building_id'] = session('building_id');
            if(Db::name('home_room_publish')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        //服务
        $service = Db::table('home_room_service')->where('building_id',session('building_id'))->select();
        //员工
        $admin = Db::table('home_room_admin')->where('building_id',session('building_id'))->select();
        //房间
        $map = [
            ['building_id','=',session('building_id')],
            ['status','<>','1'],
        ];
        $room = Db::table('room')->where($map)->select();
        return view('add_publish',['service'=>$service,'admin'=>$admin,'room'=>$room]);
    }

}
