<?php
namespace app\home\controller;


use app\index\controller\Basics;
use app\index\validate\Goodss;
use think\facade\Db;


/*
 * 统计数据
 *
 * */

class Statistic extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        $this->model_name = 'Goodss';
        $this->new_model();
        $this->validate = new Goodss();
        parent::initialize();
    }

    /*
     * 显示页面
     *
     * */
    public function index()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['building_id','=',session('building_id')],
                    ['create_time','>',$data['start']],
                    ['create_time','<',$data['end']]
                ];
            }
            if(!empty($data['name'])){
                $name = $data['name']."%";
                $map = [
                    ['building_id','=',session('building_id')],
                    ['name','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['building_id','=',session('building_id')]
            ];
        }
        $list = Db::table('goodss')
            ->where($map)
            ->paginate(['list_rows'=> 5,'query' => input('param.')]);

        return view('index',['list' => $list]);
    }

}
