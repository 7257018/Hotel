<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;
/*
 * 查看房价
 * */
class Houseprice extends Basics
{
    /*
     * 查看房价
     * */
    public function index()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['layout_id'])){
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['b.id','like',$data['layout_id']],
                ];
            }
            if(!empty($data['room_num'])){
                $name = '%'.$data['room_num']."%";
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.room_num','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list =  Db::table('room')
            ->alias('a')
            ->field('a.*,b.type_name,b.price,b.deposit,c.monday')
            ->join('layout b','a.type_id = b.id')
            ->join('week c','c.layout_id = a.id')
            ->where($map)
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        $layout = $this->select_all('layout');
        return view('index',['list' => $list,'layout' =>$layout]);
    }

}
