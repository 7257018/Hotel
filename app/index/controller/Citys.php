<?php
namespace app\index\controller;

use app\BaseController;
use think\facade\Db;


/*
 * 首页
 *
 * */

class Citys extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        parent::initialize();
    }
    /*
     * 城市首页
     *
     * */
    public function index()
    {
        $list = Db::table('admin_city')->select();
        return view('index',['list'=>$list]);
    }

    /*
     * 城市添加
     * */
    public function adds(){
        $data = input('citys');

        if(empty($data)){
            return $this->return_json('城市不能为空','0');
        }
        if(request()->isAjax()){
            $datas['citys'] = $data;
            $datas['create_time'] = time();
            if( Db::table('admin_city')->insert($datas)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
    }

    /*
     * 城市删除
     * */
    public function deletes(){
        $res = Db::table('admin_city')->where('id',input('id'))->delete();
        if($res){
            return $this->return_json('删除成功','100');
        }else{
            return $this->return_json('删除失败','0');
        }

    }
}
