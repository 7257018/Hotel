<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);
require  '../../vendor/Medoo1.6/src/Medoo.php';

use \GatewayWorker\Lib\Gateway;


/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据 
        Gateway::sendToClient($client_id, "Hello $client_id\r\n");
/*        sleep(10);
        Gateway::sendToAll("$client_id chenshihu\r\n");*/
        // 向所有人发送
        Gateway::sendToAll("$client_id login\r\n");
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message)
   {
       $data = self::overtime_room($message);
        // 向所有人发送 
        Gateway::sendToAll($data);
   }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {

       // 向所有人发送 
       GateWay::sendToAll("$client_id logout\r\n");
   }

   /*
    * 查询应到未到
    * */
    public static function subscribe(){

    }

   /*
    * 查询应离未离
    * */
    public static function overtime_room($id){
        $db = self::medoos();

        $list = $db->select("room", "*", [
            "move_time[<]" => date('Ymd',time()),
            "building_id" => $id,
            "move_time[!]" => ""
        ]);

        $count = $db->count("room", "*", [
            "move_time[<]" => date('Ymd',time()),
            "building_id" => $id,
            "move_time[!]" => ""
        ]);
        //开始加收费用的操作

        return json_encode(['list'=>$list,'count'=>$count],JSON_UNESCAPED_UNICODE);
//        var_dump(json_encode($list));
    }

    /*
     * 查询新订单
     * */
    public static function new_orders(){

    }

   /*
    * 连接数据库
    * */
    public static function medoos(){
        $database = new Medoo\Medoo([
            // required
            'database_type' => 'mysql',
            'database_name' => 'hotel',
            'server' => '127.0.0.1',
            'username' => 'root',
            'password' => 'root',

            // [optional]
            'charset' => 'utf8',
            'port' => 3306,

            // [optional] Table prefix
            'prefix' => '',

            // [optional] Enable logging (Logging is disabled by default for better performance)
            'logging' => true,

            // [optional] MySQL socket (shouldn't be used with server and port)
            'socket' => '/tmp/mysql.sock',

            // [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
            'option' => [
                PDO::ATTR_CASE => PDO::CASE_NATURAL
            ],

            // [optional] Medoo will execute those commands after connected to the database for initialization
            'command' => [
                'SET SQL_MODE=ANSI_QUOTES'
            ]
        ]);
        return $database;
    }
}
